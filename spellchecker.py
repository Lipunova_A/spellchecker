import re, codecs, pymorphy2

# Составление словаря
def make_dictionary(file_name):
    len_dict = {}
    dic_file = codecs.open(file_name, 'r', 'utf-8')
    for line in dic_file:
        words = re.findall('[ёа-я]+[-]?[ёа-я]*', line)
        #print(len(words[0]), ':\t', len(words))
        len_dict[len(words[0])] = words
    return(len_dict)
    
# Загрузка текста пользователем
def upload_text(file_name='test.txt'):
    #file_name = input('Введите путь к текстовому файлу, в котором вы хотите проверить орфографию (укажите расширение): ')
    text = codecs.open(file_name, 'r', 'utf-8')
    return(text)

# Ввод слова пользователем
def enter_word():
    word = input('Введите слово для проверки: ')
    return(word)

# Рассчет расстояния Дамерау-Левенштейна двух слов
def damerau_levenshtein(word, dic_word):
    d = {}
    for i in range(-1,len(word)+1):
        d[(i,-1)] = i+1
    for j in range(-1,len(dic_word)+1):
        d[(-1,j)] = j+1
    for i, ch1 in enumerate(word):
        for j, ch2 in enumerate(dic_word):
            d[(i, j)] = min(
                           d[(i-1, j)] + 1, # удаление символа
                           d[(i, j-1)] + 1, # вставка символа
                           d[(i-1, j-1)] + (ch1 != ch2), # замена символа
                          )
            if i > 0 and j > 0 and word[i] == dic_word[j-1] and word[i-1] == dic_word[j]:
                d[(i, j)] = min (d[(i, j)], d[i-2, j-2] + (ch1 != ch2)) # перестановка двух последовательных символов
    return(d[(len(word)-1, len(dic_word)-1)])

def search_dic(word, dic):
    dic = cut_dictionary(word, dic)
    if word not in dic:
        distance, answers = 3, []
        for dic_word in dic:
            current_distance = damerau_levenshtein(word, dic_word)
            if current_distance < distance:
                distance = current_distance
                answers = [dic_word]
            elif current_distance == distance:
                answers += [dic_word]

        frequency = sort_by_frequency(answers)
        overlap = sort_by_overlap(word, answers)
        vowel_errors = sort_by_vowel(word, answers)
              
        scores = {}
        for i in range(len(answers)):
            scores[answers[i]] = frequency[answers[i]] + overlap[answers[i]] + vowel_errors[answers[i]]
            
        sorted_scores = sorted(scores, key=lambda x: float(scores[x]), reverse=False)
        if sorted_scores:
            candidates = [sorted_scores[0]]    
            for i in range(1, len(sorted_scores)):
                if scores[sorted_scores[i]] == scores[sorted_scores[i-1]]:
                    candidates.append(sorted_scores[i])
                else:
                    break

            if len(candidates) > 1:
                freq = {}
                for candidate in candidates:
                    test = re.search(morph.parse(candidate)[0].normal_form+'\t.+?\t(.+?)\t', freq_rus)
                    if test != None:
                        freq[candidate] = test.group(1)
                    else:
                        freq[candidate] = 0
                sorted_keys = sorted(freq, key=lambda x: float(freq[x]), reverse=True)
                winner = sorted_keys[0]
            else:
                winner = candidates[0]

            return(winner)
        else:
            return(word)
    else:
        print ('Это слово не содержит ошибок или опечаток')
        return(word)
        
def cut_dictionary(word, dic):
    length, dictionary_part = len(word), []
    for l in range(length-1, length+2):
        try:
            dictionary_part += dic[l]
        except: pass
    return(dictionary_part)

def sort_by_frequency(answers):
    freq = {}
    freq_scores = {}
    for answer in answers:
        test = re.search(morph.parse(answer)[0].normal_form+'\t.+?\t(.+?)\t', freq_rus)
        if test != None:
            freq[answer] = test.group(1)
        else:
            freq[answer] = False
    sorted_keys = sorted(freq, key=lambda x: float(freq[x]), reverse=True)
    for i in range(len(sorted_keys)):
        freq_scores[sorted_keys[i]] = i+1
    return(freq_scores)

def sort_by_overlap(word, answers):
    overlap = {}
    overlap_scores = {}
    for answer in answers:
        overlap[answer] = intersection(word, answer)
    sorted_keys = sorted(overlap, key=lambda x: float(overlap[x]), reverse=True)
    if sorted_keys:
        overlap_scores[sorted_keys[0]] = 1
        for i in range(1,len(sorted_keys)):
            if overlap[sorted_keys[i]] == overlap[sorted_keys[i-1]]:
                overlap_scores[sorted_keys[i]] = overlap_scores[sorted_keys[i-1]]
            else:
                overlap_scores[sorted_keys[i]] = overlap_scores[sorted_keys[i-1]]+1     
    return(overlap_scores)

def intersection(word, answer):
    lengths = [[0 for j in range(len(answer)+1)] for i in range(len(word)+1)]
    for i, x in enumerate(word):
        for j, y in enumerate(answer):
            if x == y:
                lengths[i+1][j+1] = lengths[i][j] + 1
            else:
                lengths[i+1][j+1] = max(lengths[i+1][j], lengths[i][j+1])
    return(lengths[len(word)][len(answer)])

def expand(string):
    return(string.format(**globals()))

def sort_by_vowel(word, answers):
    orph = {}
    orph_scores = {}
    string = expand(r'[{vowels}]')
    consonant_mask = re.sub(string, '*', word)
    for answer in answers:
        if re.sub(string, '*', answer) == consonant_mask:
            for position, letter in enumerate(consonant_mask):
                if letter == '*' and word[position] != answer[position]:
                    if substitution_possible(word, answer, position) == True:
                        if answer not in orph:
                            orph[answer] = 3
                        else:
                            orph[answer] = 2
                    else:
                        orph[answer] = 1
        if answer not in orph:
            orph[answer] = 0
    sorted_keys = sorted(orph, key=lambda x: float(orph[x]), reverse=True)
    if sorted_keys:
        orph_scores[sorted_keys[0]] = 1
        for i in range(1,len(sorted_keys)):
            if orph[sorted_keys[i]] == orph[sorted_keys[i-1]]:
                orph_scores[sorted_keys[i]] = orph_scores[sorted_keys[i-1]]
            else:
                orph_scores[sorted_keys[i]] = orph_scores[sorted_keys[i-1]]+1
    return(orph_scores)

def substitution_possible(word, other_word, position):
    for precondition, substitution in substitutions:
            if re.compile(precondition).search(other_word, position, position+1):
                possible_substitutions = substitution.get(other_word[position], '')
                if word[position] in possible_substitutions + other_word[position]:
                    return(True)
                else:
                    return(False)
                    
def back_to_normal(word_no_punct, word, words):
    if word_no_punct[0].isupper():
        word = word[0].upper() + word[1:]
        
    if not word_no_punct.islower() and not word_no_punct.isupper() and not word_no_punct.isdigit():

        if len(word_no_punct)-intersection(word_no_punct.lower(), word) <= 2 and len(word_no_punct) == len(word):
            for index in range(len(word_no_punct)):
                if word_no_punct[index].isupper():
                    word = word[:index] + word[index].upper() + word[index+1:]

    if re.search('[!\"№;%:&\?\*\(\)-=_\+\[\]\{\}\'/<>,\.]', words[num]):
        start_pos = re.search('^([!\"№;%:&\?\*\(\)-=_\+\[\]\{\}\'/<>,\.]+)', words[num])
        end_pos = re.search('([!\"№;%:&\?\*\(\)-=_\+\[\]\{\}\'/<>,\.]+)$', words[num])
        if start_pos != None: 
            word = start_pos.group(1) + word
        if end_pos != None: 
            word = word + end_pos.group(1)
        if len(words[num])-intersection(words[num].lower(), word) <= 5:
            mid_pos = re.search('[\w\d]+([!\"№;%:&\?\*\(\)-=_\+\[\]\{\}\'/<>,\.]+)[\w\d]+', words[num])
            if mid_pos != None:
                for index in range(len(words[num])):
                    if words[num][index] in '!"№;%:&?*()-=_+[]{}:;\'/<>,.' and words[num][index] != word[index]:
                        if len(word_no_punct)-intersection(word_no_punct.lower(), word) <= 3:
                            word = word[:index] + words[num][index] + word[index:]
    return(word)

vowels = 'аеёиоуыэюя'
consonants = 'бвгджзйклмнпрстфхцчшщъ'
voiced_sibilant = 'жщц'
voiceless_sibilant = 'чш'

substitutions = [(expand('^[{vowels}]'), {'а': 'о', 'о': 'а', 'э': 'еиы', 'я': 'еи', 'е': 'ия', 'и': 'еэ'}),
             (expand('[{vowels}]$'), {'о': 'а',}),
             (expand('(?<=[{voiced_sibilant}])[{vowels}]'), {'о': 'а', 'и': 'еыэ', 'э': 'еиы', 'е': 'иыэ'}),
             (expand('(?<=[{voiceless_sibilant}])[{vowels}]'), {'а': 'еиэ', 'о': 'аеиы', 'и': 'е', 'е': 'и'}),
             (expand('(?<=[{consonants}])[{vowels}]'), {'а': 'о', 'о': 'а', 'ы': 'э', 'е': 'и', 'и': 'е', 'я': 'еи'})]

special_words = {'тыща':'тысяча', 
                'тыщи':'тысячи',
                'тыще':'тысяче',
                 'тыщу':'тысячу',
                 'тыщей':'тысячей',
                 'тыщами':'тысячами',
                 'тыщам':'тысячам',
                 'тыщах':'тысячах',
                 'тыщ':'тысяч',
                 'писят':'пятьдесят',
                 'чо':'что',
                 'че':'что',
                 'лутче':'лучше',
                 'чот':'что-то',
                 'чота':'что-то',
                 'чет':'что-то',
                 'чета':'что-то',
                 'ваще':'вообще',
                 'хоцца':'хочется'}

single_numbers = {'1':['одн','одно','перв','перво','первы','перва'], 
                '2':['дв','дво','дву','второ','втора', 'двух'],
                '3':['тр','тро','трех','тре','трети','треть','з'],
                '4':['четыр','четырь', 'четверт', 'четверты','четверта','четверто','ч' ],
                '5':['пять','пят','пяти','пято','пяты','пята'],
                '6':['шесть','шест','шести','шесто','шеста'],
                '7':['семь','сем','семи','седьм','седьмо','седьма'],
                '8':['восемь','восьм','восьми','восьмо','восьма'],
                '9':['девять','девят','девяти','девято','девяты','девята','я'],
                '0':['о', '']}

double_numbers = {'11':['одиннадцат','одиннадцато','одиннадцата','одиннадцаты'], 
                '12':['двенадцат','двенадцата','двенадцато','двенадцаты'],
                '13':['тринадцат','тринадцата','тринадцато','тринадцаты'],
                '14':['четырнадцат','четырнадцата','четырнадцато','четырнадцаты' ],
                '15':['пятнадцат','пятнадцата','пятнадцато','пятнадцаты'],
                '16':['шестнадцат','шестнадцата','шестнадцаты','шестнадцато'],
                '17':['семнадцат','семнадцата','семнадцаты','семнадцато'],
                '18':['восемнадцат','восемнадцата','восемнадцаты','восемнадцато'],
                '19':['девятнадцат','девятнадцата','девятнадцаты','девятнадцато'],
                '20':'двадцат', '30':'тридцат', '40':'сорок', '50':'пятьдесят', '60':'шестьдесят',
                '70':'семьдесят', '80':'восемьдесят','90':'девяносто',
                '2':'двадцать', '3':'тридцать', '4':'сорок', '5':'пятьдесят', '6':'шестьдесят',
                '7':'семьдесят', '8':'восемьдесят','9':'девяносто'}

triple_numbers = {'100':'сот', '200':'двухсот', '300':'трехсот', '400':'четырехсот', '500':'пятисот', '600':'шестисот', 
                '700':'семисот', '800':'восьмисот', '900':'девятисот',
                '1':'сто', '2':'двести', '3':'триста', '4':'четыреста', '5':'пятьсот', '6':'шестьсот', 
                '7':'семьсот', '8':'восемсот', '9':'девятьсот',}
                
print('Пожалуйста, подождите, обрабатывается словарь.')
dic = make_dictionary('russian2.dic')
freq_rus = codecs.open('freqrnc2011.csv', 'r', 'utf-8').read().lower()

morph = pymorphy2.MorphAnalyzer()

text = upload_text()
# документ utf-8 без bom
result = codecs.open('result.txt', 'w', 'utf-8')

for line in text:
    print(line)
    words = re.findall('[^\s]+', line)
    for num in range(len(words)):

        word_no_punct = re.sub('[^\w\d\r\n]+', '', words[num])
        word = word_no_punct.lower()
        print(word)
        
        if len(word) == 0 or len(word) > 32:
            result.write(words[num] + ' ')
            continue
        
        if word in dic[len(word)] or word.isdigit():
            word = back_to_normal(word_no_punct, word, words)
            result.write(word + ' ')
            continue

        elif word in special_words.keys():
            word = back_to_normal(word_no_punct, special_words[word], words)
            result.write(word + ' ')
            continue

        with_number = re.findall("([\d]+)(’|-|')?[а-яё]+", word)
        if with_number:
            if len(with_number[0][0]) == 1:
                temp = re.sub(with_number[0][0], single_numbers[(with_number[0][0])][0], word)
                if len(single_numbers[(with_number[0][0])]) > 1:
                    temp2 = re.sub(with_number[0][0] + "(’|-|')", single_numbers[(with_number[0][0])][1], word)
                    temp3 = re.sub(with_number[0][0] + "(’|-|')", single_numbers[(with_number[0][0])][2], word)
                    temp4 = re.sub(with_number[0][0] + "(’|-|')", single_numbers[(with_number[0][0])][3], word)
                    temp5 = re.sub(with_number[0][0] + "(’|-|')", single_numbers[(with_number[0][0])][4], word)
                    if len(single_numbers[(with_number[0][0])]) > 5:
                        temp6 = re.sub(with_number[0][0] + "(’|-|')", single_numbers[(with_number[0][0])][5], word)
                        if len(single_numbers[(with_number[0][0])]) > 6:
                            temp7 = re.sub(with_number[0][0] + "(’|-|')", single_numbers[(with_number[0][0])][6], word)

            elif len(with_number[0][0]) == 2:
                if int(with_number[0][0]) < 20:
                    temp = re.sub(with_number[0][0]  + "(’|-|')", double_numbers[(with_number[0][0])][0], word)
                    temp2 = re.sub(with_number[0][0] + "(’|-|')", double_numbers[(with_number[0][0])][1], word)
                    temp3 = re.sub(with_number[0][0] + "(’|-|')", double_numbers[(with_number[0][0])][2], word)
                    temp4 = re.sub(with_number[0][0] + "(’|-|')", double_numbers[(with_number[0][0])][3], word)
                else:
                    new_number = list(with_number[0][0])
                    if new_number[1] == '0':
                        print(double_numbers[with_number[0][0]])
                        temp = re.sub(with_number[0][0] + "(’|-|')", double_numbers[with_number[0][0]], word)    
                    else:
                        temp = re.sub(with_number[0][0] + "(’|-|')", double_numbers[new_number[0]] +  ' ' + single_numbers[new_number[1]][0], word)
                        temp2 = re.sub(with_number[0][0] + "(’|-|')", double_numbers[new_number[0]] +  ' ' + single_numbers[new_number[1]][1], word)
                        temp3 = re.sub(with_number[0][0] + "(’|-|')", double_numbers[new_number[0]] +  ' ' + single_numbers[new_number[1]][2], word)
                        temp4 = re.sub(with_number[0][0] + "(’|-|')", double_numbers[new_number[0]] +  ' ' + single_numbers[new_number[1]][3], word)
                        temp5 = re.sub(with_number[0][0] + "(’|-|')", double_numbers[new_number[0]] +  ' ' + single_numbers[new_number[1]][4], word)
                        if len(single_numbers[new_number[1]]) > 5:
                            print(single_numbers[new_number[1]][5])
                            temp6 = re.sub(with_number[0][0] + "(’|-|')", double_numbers[new_number[0]] + ' ' + single_numbers[new_number[1]][5], word)
                            if len(single_numbers[new_number[1]]) > 6:
                                temp7 = re.sub(with_number[0][0] + "(’|-|')", double_numbers[new_number[0]] + ' ' +  single_numbers[new_number[1]][6], word)

            elif len(with_number[0][0]) == 3:
                new_number = list(with_number[0][0])
                if int(new_number[1]) < 2:
                    temp = re.sub(with_number[0][0]  + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[(with_number[0][0])][0], word)
                    temp2 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] + ' ' +  double_numbers[(with_number[0][0])][1], word)
                    temp3 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[(with_number[0][0])][2], word)
                    temp4 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[(with_number[0][0])][3], word)
                else:
                    if new_number[1] == '0' and new_number[2] == '0':
                        temp = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[with_number[0][0]], word) 

                    elif new_number[1] == '0'and new_number[2] != '0':
                        temp = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + single_numbers[new_number[2]][0], word)
                        temp2 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] + ' ' +  single_numbers[new_number[2]][1], word)
                        temp3 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + single_numbers[new_number[2]][2], word)
                        temp4 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + single_numbers[new_number[2]][3], word)
                        temp5 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + single_numbers[new_number[2]][4], word)
                    elif new_number[1] != '0' and new_number[2] == '0':
                        temp = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[new_number[1]], word)
                        temp2 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[new_number[1]], word)
                        temp3 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[new_number[1]], word)
                        temp4 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[new_number[1]], word)
                        temp5 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[new_number[1]], word)    
                    else:    
                        temp = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[new_number[1]] +  ' ' + single_numbers[new_number[2]][0], word)
                        temp2 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] + ' ' +  double_numbers[new_number[1]] +  ' ' + single_numbers[new_number[2]][1], word)
                        temp3 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[new_number[1]] +  ' ' + single_numbers[new_number[2]][2], word)
                        temp4 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[new_number[1]] +  ' ' + single_numbers[new_number[2]][3], word)
                        temp5 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] +  ' ' + double_numbers[new_number[1]] +  ' ' + single_numbers[new_number[2]][4], word)
                        if len(single_numbers[new_number[2]]) > 5:
                            temp6 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] + ' ' + double_numbers[new_number[1]][1] +  ' ' + single_numbers[new_number[2]][5], word)
                            if len(single_numbers[new_number[2]]) > 6:
                                temp7 = re.sub(with_number[0][0] + "(’|-|')", triple_numbers[new_number[0]] + ' ' +  double_numbers[new_number[1]][1] +  ' ' + single_numbers[new_number[2]][6], word)
                if temp in dic[len(temp)]:
                    word = back_to_normal(word_no_punct, temp, words)
                    result.write(temp + ' ')
                    continue
                elif temp2 in dic[len(temp2)]:    
                    word = back_to_normal(word_no_punct, temp2, words)
                    result.write(temp2 + ' ')
                    continue
                elif temp3 in dic[len(temp3)]:    
                    word = back_to_normal(word_no_punct, temp3, words)
                    result.write(temp3 + ' ')
                    continue
                elif temp4 in dic[len(temp4)]:    
                    word = back_to_normal(word_no_punct, temp4, words)
                    result.write(temp4 + ' ')
                    continue
                elif temp5 in dic[len(temp5)]:    
                    word = back_to_normal(word_no_punct, temp5, words)
                    result.write(temp5 + ' ')
                    continue
                elif temp6 in dic[len(temp6)]:    
                    word = back_to_normal(word_no_punct, temp6, words)
                    result.write(temp6 + ' ')
                    continue
                elif temp7 in dic[len(temp7)]:    
                    word = back_to_normal(word_no_punct, temp7, words)
                    result.write(temp7 + ' ')
                    continue
                elif temp8 in dic[len(temp8)]:    
                    word = back_to_normal(word_no_punct, temp8, words)
                    result.write(temp8 + ' ')
                    continue
                else:
                    word = search_dic(word, dic)
                    word = back_to_normal(word_no_punct, word, words)
                    result.write(word + ' ')
                    continue

        elif (word.startswith('не') or word.startswith('ни') or word.startswith('то')) and word[2:] in dic[len(word[2:])]:
            word = word[:2] + ' ' + word[2:]
            word = back_to_normal(word_no_punct, word, words)
            result.write(word + ' ')
            continue

        elif (word.endswith('бы') or word.endswith('ли') or word.endswith('же')) and word[:-2] in dic[len(word[:-2])]:
            word = word[:-2] + ' ' + word[-2:]
            word = back_to_normal(word_no_punct, word, words)
            result.write(word + ' ')
            continue

        elif (word.endswith('то') or word.endswith('ка')) and word[:-2] in dic[len(word[:-2])]:
            word = word[:-2] + '-' + word[-2:]
            word = back_to_normal(word_no_punct, word, words)
            result.write(word + ' ')
            continue

        else:
            word = search_dic(word, dic)
            word = back_to_normal(word_no_punct, word, words)
            result.write(word + ' ')
            continue

    result.write('\r\n')
print('done')
result.close()